
public class Element {

	private String Id,Name,xmlFile;
	
	public Element(String Id, String Name, String xmlFile) {
		this.Id = Id;
		this.Name = Name;
		this.xmlFile = xmlFile;
	}

	public String getId() {
		return Id;
	}

	public void setId(String id) {
		Id = id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public String getXmlFile() {
		return xmlFile;
	}

	public void setXmlFile(String xmlFile) {
		this.xmlFile = xmlFile;
	}
	
	

}
