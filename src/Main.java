import java.awt.BorderLayout;
import java.awt.FileDialog;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Scanner;
import java.util.Vector;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Main extends JFrame implements ActionListener{
	
	final String ANDROID_ID = "android:id";
	Vector <Element> listElement = new Vector<>();
	private JButton btnChoose = new JButton("Choose");
	private DefaultListModel model = new DefaultListModel();
	private JList<Element> listData;
	private JScrollPane pane;
	
	public void readData(){
		FileDialog dialog = new FileDialog(this, "Select XML");
		dialog.setMode(FileDialog.LOAD);
		dialog.setVisible(true);
		
		try {
			File xmlFile = new File(dialog.getDirectory()+dialog.getFile());
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);
			doc.getDocumentElement().normalize();
			
			NodeList nList = doc.getElementsByTagName("*");
			
			listElement.removeAllElements();
			
			for(int temp = 0; temp < nList.getLength() ; temp++){
				Node nNode = nList.item(temp);
				if(nNode.getNodeType()	== Node.ELEMENT_NODE){
					org.w3c.dom.Element element = (org.w3c.dom.Element) nNode;
					listElement.add(new Element(element.getNodeName(),element.getAttribute(ANDROID_ID),xmlFile.getName()));
				}
			}

			for(Element e : listElement){
				model.addElement(e.getXmlFile()+" "+e.getName()+" "+e.getId());
			}
			
			listData.removeAll();
			listData = new JList<>(model);
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Main() {
		
		listData = new JList<>(model);
		pane = new JScrollPane(listData);
		
		add(btnChoose, BorderLayout.NORTH);
		add(pane, BorderLayout.CENTER);
		btnChoose.addActionListener(this);
		
		
		setSize(600, 400);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
	}
	
	
	
	
	public static void main(String[] args) {
		new Main();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == btnChoose){
			readData();
		}
	}
}
